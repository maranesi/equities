import {connect} from "react-redux";
import InfoCompany from "../components/InfoCompany";
import AppActions from "../actions/AppActions";

const mapStateToProps = (state) => {
  const company = state.data.company;
  return {
    visible : state.app.visibleInformation,
    title :`${company.symbol} - ${company.companyName}`,
    subheading :`${company.industry} - ${company.sector}`,
    description :company.description,
    tags :(company.tags).join(', '),
    exchange :company.exchange,
    ceo :company.CEO,
    website :company.website,
    issueType :company.issueType,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeVisible: () => dispatch(AppActions.toggleVisibleInformation()),
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(InfoCompany);
