import {connect} from "react-redux";
import Home from "./../pages/Home";
import AppActions from "../actions/AppActions";
import DataActions from "../actions/DataActions";

const mapStateToProps = (state) => {
  const textMarquee = state.data.topsData.map((item,key)=>{
    return `${item.symbol} ${item.price.toFixed(2)} -`
  }).join(' ');
  return {
    textMarquee : textMarquee,
    hasQuote: state.app.hasQuote,
    symbol: state.app.symbol,
    refresh: state.data.refresh,
    topsData : state.data.topsData,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fireStreaming: async (symbol) => {
      await dispatch(DataActions.runRefresh());
      await dispatch(AppActions.requestStarted());
      await dispatch(DataActions.getStreamingData(symbol));
    },
    getStreamingData: (symbol) => dispatch(DataActions.getStreamingData(symbol)),
    getTopsData: async () => {
      await dispatch(DataActions.getTopsData());
    },

  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
