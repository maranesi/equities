import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {BrowserRouter} from 'react-router-dom';
import styled from 'styled-components';
import NavBar from '../components/NavBar';
import {connect} from "react-redux";

const Stage = styled.div`
  width:100vw;
  padding:0;
  
  & > div {
    width:100%;
  }
`;

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <NavBar {...this.props} />
          <Stage>
            {this.props.children}
          </Stage>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

App.propTypes = {
  applicationName: PropTypes.string.isRequired,
  developerName: PropTypes.string.isRequired,
  proposal: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
  return {
    ...state.content
  }
};

export default connect(mapStateToProps, {})(App);
