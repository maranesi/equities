import Message from '../components/Message';
import {connect} from "react-redux";

const mapStateToProps = (state) => {
  return {
    ...state.app.message
  }
};

export default connect(mapStateToProps, {})(Message);
