import {connect} from "react-redux";
import CardGroup from "../components/CardGroup";

const mapStateToProps = (state) => {
  return {
    data: [{
      title: state.data.latestPrice.toFixed(2),
      description: state.data.latestTime,
      icon: 'dollar-sign'
    }, {
      title: state.data.high.toFixed(2),
      description: 'Maior valor alcançado.',
      icon: 'dollar-sign'
    }, {
      title: state.data.low.toFixed(2),
      description: 'Maior valor alcançado.',
      icon: 'dollar-sign'
    }, {
      title: state.data.open.toFixed(2),
      description: 'Valor de abertura.',
      icon: 'dollar-sign'
    }]
  }
};

export default connect(mapStateToProps, {})(CardGroup);
