import React from "react";
import {connect} from "react-redux";
import ChartBox from "../components/ChartBox";
import AppActions from "../actions/AppActions";
import moment from "moment/moment";
import DataActions from "../actions/DataActions";

const mapStateToProps = (state) => {
  const axisSelected = state.data.axisSelected;

  const props = {
    symbol: state.app.symbol,
    dateSelected: state.data.dateSelected,
    dateOptions: state.data.dateOptions,
    axisSelected: axisSelected,
    axisOptions: state.data.axisOptions,
    historyData: state.data.historyData,
    streamingData: state.data.streamingData,
    variationData: state.data.variationData,
  };

  props.nameAxis = ( props.axisOptions.find((axis) => axis.key === axisSelected).value);
  props.axis = props.axisOptions.map((item, key) => {
    return (
      <option value={item.key} key={key}>
        {item.value}
      </option>
    )
  });

  props.dates = props.dateOptions.map((item, key) => {
    return (
      <option value={item.key} key={key}>
        {item.value}
      </option>
    )
  });

  props.data = props.historyData.slice(0, 15).map((item) => {
    return {
      date: moment(item.date, 'YYYYMMDD').format('DD/MM'),
      pv: moment(item.date, 'YYYYMMDD').format('YYYYMMDD'),
      value: item[axisSelected]
    }
  });

  return props;
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateAxis: (axisSelected) => dispatch(DataActions.updateAxisSelected(axisSelected)),
    updateDate: async (symbol, dateSelected) => {
      await dispatch(DataActions.updateDateSelected(dateSelected));
      await dispatch(AppActions.requestStarted());
      await dispatch(DataActions.getHistoryData(symbol, dateSelected));
      await dispatch(AppActions.successfulRequest());
      await dispatch(AppActions.requestFinished())
    },
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(ChartBox);
