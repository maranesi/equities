import Search from './../components/Search'
import {connect} from "react-redux";
import DataActions from "../actions/DataActions";
import AppActions from "../actions/AppActions";

const mapStateToProps = (state) => {
  return {
    textButton : 'Verificar',
    textPlaceholder :'Digite a sigla da empresa que deseja cotar.',
    futureSymbol : state.app.futureSymbol,
    dateSelected : state.data.dateSelected,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChange: (symbol) => dispatch(AppActions.changeSymbol(symbol)),
    onClick: async (symbol, dateSelected) => {
      if (!symbol) {
        let message = `É necessário digitar um simbolo..`
        await dispatch(DataActions.stopRefresh());
        return await dispatch(AppActions.invalidatedInitial(message));
      }
      await dispatch(AppActions.validatedInitial(symbol));
      await dispatch(AppActions.requestStarted());
      await dispatch(DataActions.getCompanyData(symbol));
      await dispatch(DataActions.getHistoryData(symbol, dateSelected));
      const quoteData = await dispatch(DataActions.getQuoteData(symbol));
      if (!quoteData.payload) {
        let message = `Sigla não existe ou não foi possivel localizar as ações da Empresa.`
        return await dispatch(AppActions.invalidatedInitial(message));
      }
      await dispatch(AppActions.successfulRequest());
      await dispatch(AppActions.requestFinished())
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
