import {combineReducers} from 'redux'
import AppReducer from './AppReducer'
import DataReducer from './DataReducer'
import ContentReducer from './ContentReducer'

export default combineReducers({
  app: AppReducer,
  data: DataReducer,
  content: ContentReducer
})
