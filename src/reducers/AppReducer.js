import * as ActionTypes from './../actions/ActionsType';
import {Map} from 'immutable';

const initialState = Map({
  sync: false,
  hasQuote: false,
  error: true,
  message: {
    text: '',
    visible: false,
  },
  visibleInformation : true,
  symbol: 'a',
  futureSymbol: ''
});


export default (state = null, action) => {
  if (!state) {
    state = initialState.toObject();
  }

  switch (action.type) {
    case ActionTypes.CHANGE_SYMBOL:
      return {
        ...state,
        futureSymbol: action.payload
      };

    case ActionTypes.TOGGLE_VISIBLE_INFORMATION:
      return {
        ...state,
        visibleInformation: !state.visibleInformation
      };

    case ActionTypes.INVALIDATED_INITIAL:
      return {
        ...state,
        symbol: '',
        hasQuote: false,
        error: true,
        message: {
          text: action.payload,
          visible: true,
        }
      };

    case ActionTypes.VALIDATED_INITIAL:
      return {
        ...state,
        error: false,
        message: {
          text: '',
          visible: false,
        },
        symbol: action.payload
      };

    case ActionTypes.REQUEST_STARTED:
      return {...state, sync: true};

    case ActionTypes.REQUEST_FINISH:
      return {...state, sync: false};

    case ActionTypes.SUCCESSFUL_REQUEST:
      return {...state, hasQuote: true};

    default :
      return state;
  }
}
