import * as ActionTypes from './../actions/ActionsType';
import * as Config from './../config';
import {Map, List} from 'immutable';

const initialState = Map({
  latestPrice: 0,
  high: 0,
  low: 0,
  latestTime: '',
  open: 0,
  company: {},
  historyData: {},
  variationData : [],
  streamingData : [],
  topsData : [],
  refresh : false,
  dateSelected: '1m',
  dateOptions: [
    {key: '5y', value: 'Cinco anos'},
    {key: '2y', value: 'Dois anos'},
    {key: '1y', value: 'Um ano'},
    {key: '6m', value: 'Seis meses'},
    {key: '3m', value: 'Três meses'},
    {key: '1m', value: 'Um mês'}
  ],
  axisSelected: 'close',
  axisOptions: [
    {key: 'high', value: 'Maior Valor'},
    {key: 'low', value: 'Menor Valor'},
    {key: 'open', value: 'Valor de Abertura'},
    {key: 'close', value: 'Ultimo Preço'},
  ],
});

export default (state = null, action) => {
  if (!state) {
    state = initialState.toObject();
  }

  switch (action.type) {
    case ActionTypes.UPDATED_AXIS_SELECTED:
      return {...state, axisSelected: action.payload};

    case ActionTypes.UPDATED_DATE_SELECTED:
      return {...state, dateSelected: action.payload};

    case ActionTypes.MAKE_QUOTE:
      return {
        ...state,
        latestPrice: action.payload.latestPrice,
        high: action.payload.high,
        low: action.payload.low,
        latestTime: action.payload.latestTime,
        open: action.payload.open,
      };

    case ActionTypes.RESET_QUOTE:
      return {...state, latestPrice: {}};

    case ActionTypes.MAKE_COMPANY:
      return {...state, company: action.payload};

    case ActionTypes.RESET_STREAMING_DATA:
      return {...state, realtimeData: [] };

    case ActionTypes.RESET_COMPANY:
      return {...state, company: {}};

    case ActionTypes.MAKE_HISTORY:
      return {...state, historyData: action.payload};

    case ActionTypes.RESET_HISTORY:
      return {...state, historyData: {}};

    case ActionTypes.MAKE_TOPS:
      return {...state, topsData: action.payload};

    case ActionTypes.RESET_TOPS:
      return {...state, topsData: {}};

    case ActionTypes.RUN_REFRESH:
      return {...state, refresh: true };

    case ActionTypes.STOP_REFRESH:
      return {...state, refresh: false };

    case ActionTypes.MAKE_STREAMING_DATA:
      let variationData = List(state.variationData);
      let streamingData = List(state.streamingData);

      if( variationData.count() > Config.COLUMNS_LIMIT ){
        variationData = variationData.shift();
      }
      if( streamingData.count() > Config.COLUMNS_LIMIT ){
        streamingData = streamingData.shift();
      }
      variationData = variationData.push( {value:action.payload.change});
      streamingData = streamingData.push( {value:action.payload.latestPrice});

      return {
        ...state,
        variationData: variationData.toArray(),
        streamingData: streamingData.toArray()
      };

    default :
      return state;
  }
}
