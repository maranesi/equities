import React from 'react';
import PropTypes from 'prop-types';
import {CSSTransitionGroup} from 'react-transition-group'
import styled from 'styled-components';
import ChartBoxContainer from '../containers/ChartBoxContainer';
import SearchContainer from '../containers/SearchContainer';
import MessageContainer from '../containers/MessageContainer';
import InfoCompanyContainer from '../containers/InfoCompanyContainer';
import CardGroupContainer from '../containers/CardGroupContainer';
import Marquee from 'react-marquee';

const BoxSearchQuote = styled.div`
  width:100%;
  padding:8px 16px;
  display:flex;
  flex-direction:column;
`;

const MarqueeBox = styled.div`
  align-items:center;
  background:#FAFAFA;
  color:#666;
  display:flex;
  font-size:0.8em;
  height:40px;
  margin:0;
  width:100%;
`;


const Details = styled.div`
  height:calc(100% - 200px);
  margin:20px 0 70px;
  min-width:400px;
  width:100%;
      
  & > div {
    width:100%;
  }
  
  &.active > div {
    display:block;
  }
`;

const InfoQuote = styled.section`
  align-items:center;
  display:flex;
  justify-content:space-between;
  width:100%;
`;

let timer;

class Home extends React.Component {

  componentDidUpdate() {
    const {hasQuote, refresh, symbol, fireStreaming, getStreamingData} = this.props;
    if (hasQuote && !refresh && !timer) {
      fireStreaming(symbol);
      timer = setInterval(() => {
        getStreamingData(symbol)
      }, 3000)
    }
    if (!this.props.hasQuote) {
      clearInterval(timer);
    }
  }

  componentDidMount(){
    if(!this.props.topsData.length){
      this.props.getTopsData();
    }
  }

  render() {
    let contentDetails;
    const {hasQuote, textMarquee} = this.props;

    if (hasQuote) {
      contentDetails = (
        <React.Fragment>
          <InfoCompanyContainer/>
          <InfoQuote>
            <CardGroupContainer/>
          </InfoQuote>
          <ChartBoxContainer/>
        </React.Fragment>
      )
    }

    return (
      <React.Fragment>
        <MarqueeBox>
          <Marquee loop hoverToStop text={textMarquee} />
        </MarqueeBox>
        <BoxSearchQuote>
          <SearchContainer/>
          <MessageContainer/>
          <Details>
            <CSSTransitionGroup
              component='div'
              transitionName='fade'
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
            >
              {contentDetails}
            </CSSTransitionGroup>
          </Details>
        </BoxSearchQuote>
      </React.Fragment>

    );
  }
}

Home.propTypes = {
  hasQuote: PropTypes.bool.isRequired,
  symbol: PropTypes.string.isRequired,
  textMarquee: PropTypes.string.isRequired,
  refresh: PropTypes.bool.isRequired,
  fireStreaming: PropTypes.func.isRequired,
  getStreamingData: PropTypes.func.isRequired,
  getTopsData: PropTypes.func.isRequired,
};

export default Home;
