import React from 'react';
import {Route} from 'react-router-dom';

import HomeContainer from './containers/HomeContainer';

export default class Router extends React.Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={HomeContainer}/>
      </div>
    );
  }
}
