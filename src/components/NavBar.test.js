import React from 'react';
import configureMockStore from 'redux-mock-store';
import Adapter from 'enzyme-adapter-react-16';
import {shallow} from 'enzyme';
import {configure} from 'enzyme';
import NavBar from './NavBar';

configure({adapter: new Adapter()});

const mockStore = configureMockStore();

describe('test NavBar', () => {
  let wrapper, store;
  let applicationName = 'Teste';

  beforeEach(() => {
    const initialState = {
      app: {
        applicationName
      }
    };
    store = mockStore(initialState);
    wrapper = shallow(
      <NavBar store={store}/>
    );
  });

  it('should show the value of applicationName', () => {
    expect(wrapper.props().applicationName).toBe(applicationName);
  });

  it('should see the name equal to the tag', () => {
    expect(wrapper.name()).toBe('NavBar');
  });

});

