import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

const Quart = styled.div`
  align-items:center;  
  background:#FFF;
  border:1px solid #f4f4f4;
  border-radius:4px;
  box-shadow: 0 0 10px #e4e4e4;
  display:flex;
  justify-content:space-around;
  margin:8px 4px;
  padding:12px;
  width:calc(25% - 8px);
`;

const Wrapper = styled.span`
  color:rgba(0,0,0,.1);
  font-size:30px;
  width:30px;
`;

const Content = styled.div`
  display:flex;
  flex-direction:column;
  justify-content:flex-end;
  align-items:flex-end;
  width:calc(100% - 30px);
`;

const Value = styled.span`
  color:#424242;
  font-size:1.8em;
`;

const Description = styled.span`
  color:#666;
  font-size:0.8em;
`;

const Card = ({title, description, icon}) => (
  <Quart>
    <Wrapper>
      <FontAwesomeIcon icon={icon}/>
    </Wrapper>
    <Content>
      <Value> {title} </Value>
      <Description> {description} </Description>
    </Content>
  </Quart>
);

Card.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

export default Card;
