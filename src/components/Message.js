import React from 'react';
import PropTypes from 'prop-types';
import styled, {keyframes} from 'styled-components';
import classNames from 'classnames';

const show = keyframes`
  from {
    opacity:0;
    margin-top: -30px;
  }
  to {
    opacity:1;
    margin-top: 0%;
  }
`;

const Box = styled.section`
  align-items:center;
  background:#FCE4EC;
  border-radius:4px;
  color:#424242;
  display:none;
  font-size:0.9em;
  margin:5px 0;
  padding:8px 16px;
  width:100%;
  
  &.active{
    display:flex;
    animation: ${show} 0.3s linear;
  }
`;

const Message = ({visible, text}) => (
  <Box className={classNames('', visible && 'active')}>
    {text}
  </Box>
);

Message.propTypes = {
  visible: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
};

export default Message;
