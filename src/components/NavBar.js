import React from 'react';
import styled from 'styled-components';

const Bar = styled.div`
  width:100vw;
`;

const Header = styled.header`
  width:100vw;
  height:20px;
  padding:0 16px;
  background:#111;
  border-bottom:1px solid #111;
  display:flex;
  align-items:center;
  justify-content:space-between
  color:#FFF;
  font:11px "Roboto", Arial;
`;

const Section = styled.section`
  width:100vw;
  height:50px;
  display:flex;
  align-items:center;
  justify-content:center;
  background:#FFF;
  border-bottom:2px solid #fcfcfc;
`;

const NavBrand = styled.h1`
  font-family: "Raleway", Arial;
  margin:0 16px;
  font-size:24px;
`;

const NavBar = ({applicationName, developerName, proposal}) => (
  <Bar>
    <Header>
      <span>{proposal}</span>
      <span>{developerName}</span>
    </Header>
    <Section>
      <NavBrand>
        {applicationName}
      </NavBrand>
    </Section>
  </Bar>
);

export default NavBar
