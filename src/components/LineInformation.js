import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Box = styled.div`
  height:20px;
  display:flex;
  align-items:center;
  font-size:0.8em;
  border-bottom:1px solid #FAFAFA
`;

const TextLabel = styled.section`
  width:100px;
`;

const TextValue = styled.section`
  width:calc(100% - 150px);
`;

class LineInformation extends React.Component {
  render() {
    const {title, value} = this.props;

    return (
      <Box>
        <TextLabel>{title}</TextLabel>
        <TextValue>{value}</TextValue>
      </Box>
    );
  }
}

LineInformation.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

export default LineInformation;
