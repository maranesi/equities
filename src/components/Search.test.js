import React from 'react';
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import {mount} from 'enzyme';
import Search from './Search';

describe('test Search', () => {
  let textCallToAction = 'Verificar';
  let textPlaceholder = 'Digite uma sigla';

  const wrapper = mount(
    <Search
      textCallToAction={textCallToAction}
      textPlaceholder={textPlaceholder}
    />
  );

  it('should see two inputs', () => {
    expect(wrapper.find('input').length).toBe(2)
  });

  it('should see one input text', () => {
    expect(wrapper.find('input[type="text"]').length).toBe(1)
  });

  it('should see if placeholder attribute is ok', () => {
    expect(wrapper.find('input[type="text"]').prop('placeholder')).toBe(textPlaceholder)
  });

  it('should see one input text', () => {
    expect(wrapper.find('input[type="button"]').length).toBe(1)
  });

  it('should see if text of the callToAction is ok', () => {
    expect(wrapper.find('input[type="button"]').prop('value')).toBe(textCallToAction)
  });
});

