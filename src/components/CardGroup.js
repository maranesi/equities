import React from 'react';
import PropTypes from 'prop-types';
import Card from './Card'

const CardGroup = ({data}) => {
  const cards = data.map((item, key) => (<Card {...item} key={key}/>));
  return (
    <React.Fragment>
      {cards}
    </React.Fragment>
  );
};

CardGroup.propTypes = {
  data: PropTypes.array.isRequired,
};

export default CardGroup;
