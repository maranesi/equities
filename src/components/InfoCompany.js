import React from 'react';
import PropTypes from 'prop-types';
import styled, {keyframes} from 'styled-components';
import LineInformation from './LineInformation';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import classNames from "classnames";

const show = keyframes`
  from {
    opacity:0;
    margin-top: -30px;
  }
  to {
    opacity:1;
    margin-top: 0%;
  }
`;

const Box = styled.section`
  border-top:3px solid #E91E63;
  margin:10px 0;
  padding:5px;
  width:100%;
`;
const Header = styled.header`
  display:flex;
  justify-content:space-between
  width:100%;
`;

const Content = styled.section`
  width:calc(100% - 200px);
`;

const Title = styled.h6`
  color:#212121;
  font:1.3em "Roboto";
  margin:5px 0 2px;
  padding:0;
`;

const Subheading = styled.p`
  color:#424242;
  font-size:0.9em
  margin:2px 0;
  padding:0;
`;

const Navigation = styled.aside`
  align-items:center;
  color:#424242;
  display:flex;
  font-size:14px;
  justify-content:flex-end;
  padding:5px 0px;
  margin:0;
  width:30px;
  
  & > svg{
    color:#CCC;
    cursor:pointer;
    border:1px solid #CCC;
    border-radius:4px;
    padding:10px 14px;
    transition:all 0.3s ease;
  }
  
  & > svg.active{
    color:#666;
    cursor:pointer;
    border:1px solid #666;
    border-radius:4px;
    padding:10px 14px;
    transition:all 0.3s ease;
  }
  
  & > svg:hover, & > svg.active:hover{
    border:1px solid #212121;
    color:#212121;
  }
`;

const Section = styled.section`
  background:#FFF;
  border:1px solid #f4f4f4;
  border-radius:4px;
  box-shadow: 0 0 10px #e4e4e4;
  display:none;
  margin:15px 0 0;
  padding:16px;
  
  &.active{
    display:block;
    animation: ${show} 0.3s linear;
  }
`;

const Description = styled.p`
  font-size:0.80em;
  line-height:1.3em;
  margin:3px 0;
`;

const InfoCompany = ({
     visible, title, subheading, description, tags, exchange, ceo, website, issueType, onChangeVisible
   }) => (
  <Box>
    <Header>
      <Content>
        <Title> {title} </Title>
        <Subheading> {subheading} </Subheading>
      </Content>
      <Navigation>
        <FontAwesomeIcon
          onClick={onChangeVisible}
          className={classNames('', visible && 'active')} icon="info"
        />
      </Navigation>
    </Header>
    <Section className={classNames('', visible && 'active')}>
      <Description> {description} </Description>
      <LineInformation title='Tags' value={tags}/>
      <LineInformation title='Cambio' value={exchange}/>
      <LineInformation title='CEO' value={ceo}/>
      <LineInformation title='Site' value={website}/>
      <LineInformation title='Tipo' value={issueType}/>
    </Section>
  </Box>
);

InfoCompany.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  subheading: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  tags: PropTypes.string.isRequired,
  exchange: PropTypes.string.isRequired,
  ceo: PropTypes.string.isRequired,
  website: PropTypes.string.isRequired,
  issueType: PropTypes.string.isRequired,
};

export default InfoCompany;
