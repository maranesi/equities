import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const BoxSearch = styled.div`
  align-items:center;
  display:flex;
  margin:0 0 5px;
  padding:0 1px;
  width:100%;
`;

const Display = styled.label`
  color:#212121;
  display:block;
  font-size:18px;
  margin:5px 0;
  text-align:left;
  width:150px;
`;

const Input = styled.input`
  border:1px solid #E4E4E4;
  border-right:none;
  font-size:0.9em;
  height:40px;
  padding:0 16px;
  width:calc(100% - 250px);
`;

const Button = styled.input`
  border-left:none;
  background:#D12863;
  border:1px solid #DB467A;
  border-left:none;
  border-radius:0 4px 4px 0;
  color:#FFF;
  cursor:pointer
  font-size:0.9em;
  height:40px;
  padding:0 16px;
  width:100px;
`;

const Search = ({textButton, textPlaceholder, futureSymbol, onChange, onClick, dateSelected}) => (
  <BoxSearch>
    <Display>
      Buscar Cotações
    </Display>
    <Input
      className='search'
      placeholder={textPlaceholder}
      maxLength={4}
      type='text'
      onChange={(evt) => onChange(evt.target.value)}
      onKeyUp={(evt) => (evt.key).toLowerCase() === 'enter' ? onClick : true}
      value={futureSymbol}
    />
    <Button
      type='button'
      value={textButton}
      onClick={()=>{onClick( futureSymbol, dateSelected)}}
    />
  </BoxSearch>
);

Search.propTypes = {
  futureSymbol: PropTypes.string.isRequired,
  textButton: PropTypes.string.isRequired,
  textPlaceholder: PropTypes.string.isRequired,
  dateSelected: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Search;
