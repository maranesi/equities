import React from 'react';
import {AreaChart, Area, YAxis, CartesianGrid} from 'recharts';
import PropTypes from 'prop-types';

const width = window.innerWidth - 50;

const RealtimeChart = ({name, data}) => {
  return (
    <AreaChart
      width={width}
      height={70}
      data={data}
      margin={{top: 5, right: 30, left: 20, bottom: 5}}
    >
      <YAxis/>
      <CartesianGrid strokeDasharray="5 5"/>
      <Area
        type='monotone'
        dataKey='value'
        stroke='#8884d8'
        fill='#8884d8'
        dot={{stroke: '#212121', strokeWidth: 2}}
        isAnimationActive={true}
        animationEasing={'ease'}
        animationDuration={1500}
      />
    </AreaChart>
  )
}

RealtimeChart.propTypes = {
  data: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
};

export default RealtimeChart;
