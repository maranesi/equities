import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Chart from './Chart';
import RealtimeChart from './RealtimeChart';
import VariationChart from "./VariationChart";
import SelectBox from "./SelectBox";

const Box = styled.div`
  background:#FFF;
  border:1px solid #f4f4f4;
  border-radius:4px;
  box-shadow: 0 0 10px #e4e4e4;
  color:#424242;
  padding:16px;
  margin:10px 0;
  width:calc(100% - 2px);
`;

const Header = styled.div`
  align-items:center;
  display:flex;
  justify-content:space-between;
  margin:0 0 10px 0;
  width:100%;
`;

const Title = styled.h6`
  font-size:1em;
  font-weight:100;
  padding:0;
  margin:0;
  width:250px;
`;

const Navigation = styled.div`
  width:calc(100% - 250px);
  margin:0;
`;

const TargetSwitch = styled.div`
  align-items:center;
  border-radius:4px;
  display:flex;
  justify-content:flex-end;
  width:calc(100%);
`;

const Divider = styled.div`
  width:100%;
  height:20px;
`;

class ChartBox extends React.Component {
  render() {
    const {
      axisSelected,
      dateSelected,
      streamingData,
      variationData,
      updateAxis,
      updateDate,
      data,
      dates,
      axis,
      nameAxis,
      symbol,
    } = this.props;

    return (
      <Box>
        <Header>
          <Title>
            Dia x {nameAxis}
          </Title>
          <Navigation>
            <TargetSwitch>
              <SelectBox
                onUpdate={updateAxis}
                selected={axisSelected}
                options={axis}
              />
              <SelectBox
                onUpdate={(dateSelected) => {
                  console.log(symbol);
                  return updateDate(symbol, dateSelected)
                }}
                selected={dateSelected}
                options={dates}
              />
            </TargetSwitch>
          </Navigation>
        </Header>
        <Chart data={data} name={nameAxis}/>

        <Divider/>
        <Header>
          <Title>
            Realtime Ultimo Preço
          </Title>
        </Header>
        <RealtimeChart data={streamingData} name='Ultimo Preço'/>

        <Divider/>
        <Header>
          <Title>
            Realtime (Variação de Preço)
          </Title>
        </Header>
        <VariationChart data={variationData} name='Variação'/>
      </Box>
    );
  }
}

ChartBox.propTypes = {
  symbol: PropTypes.string.isRequired,
  axisSelected: PropTypes.string.isRequired,
  dateSelected: PropTypes.string.isRequired,
  axisOptions: PropTypes.array.isRequired,
  dateOptions: PropTypes.array.isRequired,
  historyData: PropTypes.array.isRequired,
  updateAxis: PropTypes.func.isRequired,
  updateDate: PropTypes.func.isRequired,
  streamingData: PropTypes.array.isRequired,
  variationData: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  dates: PropTypes.array.isRequired,
  axis: PropTypes.array.isRequired,
  nameAxis: PropTypes.string.isRequired,
};

export default ChartBox
