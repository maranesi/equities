import React from 'react';
import {AreaChart, Area, YAxis, CartesianGrid} from 'recharts';
import PropTypes from 'prop-types';

const width = window.innerWidth - 50;

const VariationChart = ({name, data}) => {
  const gradientOffset = () => {
    const dataMax = Math.max(...data.map((i) => i.value));
    const dataMin = Math.min(...data.map((i) => i.value));
    if (dataMax < 0) {
      return 0
    }
    else if (dataMin >= 0) {
      return 1
    }
    else {
      return dataMax / (dataMax - dataMin);
    }
  }
  const off = gradientOffset();

  return (
    <AreaChart
      isAnimationActive={true}
      width={width}
      height={70}
      data={data}
      margin={{top: 5, right: 30, left: 20, bottom: 5}}
    >
      <defs>
        <linearGradient id="split" x1="0" y1="0" x2="0" y2="1">
          <stop offset={off} stopColor="#E8F5E9" stopOpacity={1}/>
          <stop offset={off} stopColor="#FFCDD2" stopOpacity={1}/>
        </linearGradient>
      </defs>
      <YAxis/>
      <CartesianGrid strokeDasharray="3 3"/>
      <Area
        type="monotone"
        dataKey="value"
        stroke="#000"
        dot={{stroke: '#212121', strokeWidth: 2}}
        fill="url(#split)"
        isAnimationActive={true}
        animationEasing={'ease'}
        animationDuration={1500}
      />
    </AreaChart>
  )
}

VariationChart.propTypes = {
  data: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
};

export default VariationChart;
