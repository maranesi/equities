import React from 'react';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import PropTypes from 'prop-types';

const width = window.innerWidth - 50;

const Chart = ({name, data}) => (
  <LineChart
    width={width}
    height={230}
    data={data}
    margin={{top: 5, right: 30, left: 20, bottom: 5}}
  >
    <Line
      name={name}
      type='monotone'
      dataKey='value'
      stroke='#D12863'
      activeDot={{r: 8}}
    />
    <CartesianGrid strokeDasharray='2 2'/>
    <Tooltip/>
    <YAxis width={20} tick={{fontSize: 12}}/>
    <XAxis dataKey='date' tick={{fontSize: 12}}/>
    <Legend/>
  </LineChart>
);

Chart.propTypes = {
  data: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
};

export default Chart;
