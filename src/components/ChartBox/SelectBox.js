import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Box = styled.div`
  width:150px; 
  overflow:hidden;
  margin:0 5px;
  
  & > select {
      color:#424242;
      background:#f0f0f0;
      border:1px solid #e4e4e4;
      border-radius:4px;
      font-size:0.9em;
      height:40px;
      margin:0;
      padding:8px;
      text-indent:0.01px;
      text-overflow:'';      
      width:150px;
  }
  
  & > select::-ms-expand {
    display: none
  };
`;

const SelectBox = ({selected, options, onUpdate}) => (
  <Box>
    <select
      defaultValue={selected}
      onChange={(evt) => onUpdate(evt.target.value)}
    >
      {options}
    </select>
  </Box>
);

SelectBox.propTypes = {
  selected: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onUpdate: PropTypes.func.isRequired,
};

export default SelectBox
