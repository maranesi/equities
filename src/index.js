import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import Router from './router';
import {Provider} from 'react-redux'
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import generateStore from './store'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faInfo, faDollarSign } from '@fortawesome/free-solid-svg-icons'

library.add(faInfo)
library.add(faDollarSign)

const store = generateStore();

ReactDOM.render((
  <Provider store={store}>
    <App>
      <Router/>
    </App>
  </Provider>
), document.getElementById('root'));

registerServiceWorker();
