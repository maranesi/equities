import * as ActionTypes from './ActionsType';
import axios from 'axios';

// Quote
function fetchQuote(symbol) {
  return axios.get(`https://api.iextrading.com/1.0/stock/${symbol}/quote`)
}

function makeQuote(quote) {
  return {
    type: ActionTypes.MAKE_QUOTE,
    payload: quote
  }
}

function handleRequestFailedQuote(error) {
  return {
    type: ActionTypes.RESET_QUOTE,
  }
}

// Company
function fetchCompany(symbol) {
  return axios.get(`https://api.iextrading.com/1.0/stock/${symbol}/company`)
}

function makeCompany(company) {
  return {
    type: ActionTypes.MAKE_COMPANY,
    payload: company
  }
}

function handleRequestFailedCompany(error) {
  return {
    type: ActionTypes.RESET_COMPANY,
  }
}

// History
function fetchHistory(symbol, dateSelected) {
  return axios.get(`https://api.iextrading.com/1.0/stock/${symbol}/chart/${dateSelected}`)
}

function makeHistory(historyData) {
  return {
    type: ActionTypes.MAKE_HISTORY,
    payload: historyData
  }
}

function handleRequestFailedHistory(error) {
  return {
    type: ActionTypes.RESET_HISTORY,
  }
}

// Streaming
function makeStreamingData(quote) {
  return {
    type: ActionTypes.MAKE_STREAMING_DATA,
    payload: quote
  }
}

// Top
function makeTopData(quote) {
  return {
    type: ActionTypes.MAKE_TOPS,
    payload: quote
  }
}

function fetchTops() {
  return axios.get(`https://api.iextrading.com/1.0/tops/last`)
}


// Actions creators
const actions = {};

actions.getQuoteData = (symbol) => {
  return async (dispatch) => {
    return fetchQuote(symbol).then(
      response => dispatch(makeQuote(response.data)),
      error => dispatch(handleRequestFailedQuote(error))
    );
  };
};

actions.getCompanyData = (symbol) => {
  return async (dispatch) => {
    return fetchCompany(symbol).then(
      response => dispatch(makeCompany(response.data)),
      error => dispatch(handleRequestFailedCompany(error))
    );
  };
};

actions.getHistoryData = (symbol, dateSelected) => {
  return async (dispatch) => {
    return fetchHistory(symbol, dateSelected).then(
      response => dispatch(makeHistory(response.data)),
      error => dispatch(handleRequestFailedHistory(error))
    );
  };
};

actions.getStreamingData = (symbol) => {
  return async (dispatch) => {
    return fetchQuote(symbol).then(
      response => dispatch(makeStreamingData(response.data))
    );
  };
};

actions.getTopsData = () => {
  return async (dispatch) => {
    return fetchTops().then(
      response => dispatch(makeTopData(response.data.slice(0, 30)))
    );
  };
};

actions.updateAxisSelected = (axisSelected) => {
  return {
    type: ActionTypes.UPDATED_AXIS_SELECTED,
    payload: axisSelected
  }
};

actions.updateDateSelected = (dateSelected) => {
  return {
    type: ActionTypes.UPDATED_DATE_SELECTED,
    payload: dateSelected
  }
};

actions.runRefresh = () => {
  return {type: ActionTypes.RUN_REFRESH}
};

actions.stopRefresh = () => {
  return {type: ActionTypes.STOP_REFRESH}
};

export default actions;

