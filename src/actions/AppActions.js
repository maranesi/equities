import * as ActionTypes from './ActionsType';

const actions = {};

actions.changeSymbol = (symbol) => {
  return {type: ActionTypes.CHANGE_SYMBOL, payload: symbol}
};

actions.toggleVisibleInformation = () => {
  return {type: ActionTypes.TOGGLE_VISIBLE_INFORMATION}
};

actions.requestStarted = () => {
  return {type: ActionTypes.REQUEST_STARTED}
};

actions.requestFinished = () => {
  return {type: ActionTypes.REQUEST_FINISH}
};

actions.validatedInitial = (symbol) => {
  return {type: ActionTypes.VALIDATED_INITIAL, payload: symbol}
};

actions.invalidatedInitial = (message) => {
  return {type: ActionTypes.INVALIDATED_INITIAL, payload: message}
};

actions.successfulRequest = () => {
  return {type: ActionTypes.SUCCESSFUL_REQUEST}
};

export default actions;
